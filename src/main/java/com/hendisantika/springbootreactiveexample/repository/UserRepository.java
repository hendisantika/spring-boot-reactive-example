package com.hendisantika.springbootreactiveexample.repository;

import com.hendisantika.springbootreactiveexample.entity.Users;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-reactive-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 27/04/18
 * Time: 08.04
 * To change this template use File | Settings | File Templates.
 */
public interface UserRepository extends ReactiveMongoRepository<Users, Integer> {

    Flux<Users> findByName(String name);

}
